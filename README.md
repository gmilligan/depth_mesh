# What is this?

Why is 2D drawing so easy compared to 3D drawing? 

Have you ever just wished 3D was easier? 

Here is an experiment to make it easy to see 2D depth maps translate into 3D objects, in real time. 

# Install

`npm install`

# Run

`node server.js`

# Run Tests

`npm test`
