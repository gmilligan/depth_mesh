const calledMock = jest.fn();

const FAKE_CONTEXT2D = {
    getCalledMock: () => calledMock,
    beginPath: () => {
        calledMock('beginPath');
    },
    setLineDash: (arr) => true,
    rect: (x, y, width, height) => {
        calledMock('rect', x, y, width, height);
    },
    stroke: () => {
        calledMock('stroke');
    },
};

if (typeof exports !== 'undefined') { 
    for(var k in FAKE_CONTEXT2D) exports[k]=FAKE_CONTEXT2D[k];
}