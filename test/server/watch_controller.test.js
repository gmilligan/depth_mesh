const exp = require('../../server');
const server = exp.server;
const app = exp.app;

const supertest = require('supertest');
const request = supertest(app);

const fs = require('fs');

const rootPath = __dirname+'/../../pub/';

const validPath = 'img/Test/eskimo.png';
const doesNotExistPath = 'img/Test/doesNotExist.png';
const notAFilePath = 'img/Test/notAFile';
const invalidFileTypePath = notAFilePath + '/hello.txt';

beforeAll(() => {
    server.close();
});

describe('/newwatch POST ', () => {

    it('responds with status 200', async done => {
        const response = await request.post('/newwatch');
        expect(response.status).toBe(200);
        done();
    });

    it('says already watching when duplicate watch', async done => {
        await request
            .post('/newwatch')
            .send({path: validPath, isWatching: false});
        const response2 = await request
            .post('/newwatch')
            .send({path: validPath, isWatching: true});
        
        const actual = JSON.parse(response2.text);
        const expected = {
            status: 'error - already watching', 
            path: validPath
        };

        expect(expected).toEqual(actual);
        done();
    });

    it('unwatches previous files when isWatching set to false', async done => {
        await request
            .post('/newwatch')
            .send({path: validPath, isWatching: false});
        const response2 = await request
            .post('/newwatch')
            .send({path: validPath, isWatching: false});
        
        const actual = JSON.parse(response2.text);
        const expected = {
            status: 'ok', 
            path: validPath
        };

        expect(expected).toEqual(actual);
        done();
    });

    it('sends watched path when path is valid', async done => {
        const response = await request
            .post('/newwatch')
            .send({path: validPath, isWatching: false});
        
        const actual = JSON.parse(response.text);
        const expected = {
            status: 'ok', 
            path: validPath
        };

        expect(expected).toEqual(actual);
        done();
    });

    it('says no path when missing path', async done => {
        const response = await request
            .post('/newwatch')
            .send({path: '', isWatching: false});
        
        const actual = JSON.parse(response.text);
        const expected = {
            status: 'error - no path', 
            path: ''
        };

        expect(expected).toEqual(actual);
        done();
    });

    it('says when file does not exist', async done => {
        const response = await request
            .post('/newwatch')
            .send({path: doesNotExistPath, isWatching: false});
        
        const actual = JSON.parse(response.text);
        const expected = {
            status: 'error - file does not exist', 
            path: doesNotExistPath
        };

        expect(expected).toEqual(actual);
        done();
    });

    it('says when not a file', async done => {
        const response = await request
            .post('/newwatch')
            .send({path: notAFilePath, isWatching: false});
        
        const actual = JSON.parse(response.text);
        const expected = {
            status: 'error - not a file', 
            path: notAFilePath
        };

        expect(expected).toEqual(actual);
        done();
    });

    it('says when file type is not allowed', async done => {
        const response = await request
            .post('/newwatch')
            .send({path: invalidFileTypePath, isWatching: false});
        
        const actual = JSON.parse(response.text);
        const expected = {
            status: 'error - file type not allowed', 
            path: invalidFileTypePath
        };

        expect(expected).toEqual(actual);
        done();
    });

});

describe('/pingwatch GET ', () => {

    it('responds with status 200', async done => {
        const response = await request.get('/pingwatch');
        expect(response.status).toBe(200);
        done();
    });

    it('shows watched change array after watch is first added', async done => {
        await request
            .post('/newwatch')
            .send({path: validPath, isWatching: false});

        const pingResponse = await request.get('/pingwatch');

        const actual = JSON.parse(pingResponse.text);

        expect('ok').toEqual(actual.status);
        expect(1).toEqual(actual.changes.length);
        expect(validPath).toEqual(actual.changes[0].path);
        done();
    });

    it('sends empty changes array when watching zero files', async done => {
        const response = await request.get('/pingwatch');

        const actual = JSON.parse(response.text);
        const expected = {
            status: 'ok', 
            changes: []
        };

        expect(expected).toEqual(actual);
        done();
    });

    it('shows empty changes array after first ping', async done => {
        await request
            .post('/newwatch')
            .send({path: validPath, isWatching: false});

        await request.get('/pingwatch');
        const pingResponse2 = await request.get('/pingwatch');

        const actual = JSON.parse(pingResponse2.text);

        const expected = {
            status: 'ok', 
            changes: []
        };

        expect(expected).toEqual(actual);
        done();
    });

    const modifyValidFile = () => {
        const newModifiedDate = new Date();
        newModifiedDate.setTime(newModifiedDate.getTime() + 60 * 60 * 1000);
        fs.utimesSync(rootPath + validPath, newModifiedDate, newModifiedDate);
    };

    it('can detect when file is modified', async done => {
        await request
            .post('/newwatch')
            .send({path: validPath, isWatching: false});
        await request.get('/pingwatch'); // consume the initial modified state of the watched file

        modifyValidFile(); // <-- modify the file

        const responseAfterModified = await request.get('/pingwatch');
        const actual = JSON.parse(responseAfterModified.text);

        expect('ok').toEqual(actual.status);
        expect(1).toEqual(actual.changes.length);
        expect(validPath).toEqual(actual.changes[0].path);
        done();
    });

});