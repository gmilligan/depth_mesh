const DRAW_CANVAS_2D = require('../../../../pub/draw/js/draw_canvas2d');

describe('addEvent ', () => {

    it('calls load event on init', () => {
        const calledLoad = jest.fn();
        DRAW_CANVAS_2D.addEvent('load', (args, e)=>{
            calledLoad();
        });

        DRAW_CANVAS_2D.initOnce();

        expect(calledLoad).toBeCalled();
    });

    it('calls all load events on init', () => {
        const calledLoad1 = jest.fn();
        DRAW_CANVAS_2D.addEvent('load', (args, e)=>{
            calledLoad1();
        });
        const calledLoad2 = jest.fn();
        DRAW_CANVAS_2D.addEvent('load', (args, e)=>{
            calledLoad2();
        });

        DRAW_CANVAS_2D.initOnce();

        expect(calledLoad1).toBeCalled();
        expect(calledLoad2).toBeCalled();
    });

});