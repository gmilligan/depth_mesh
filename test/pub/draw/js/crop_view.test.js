const DRAW_CROP_VIEW = require('../../../../pub/draw/js/crop_view');
const fake_context2d = require('../../../_fake/context2d');

const fake_canvas_class = {
    canvas_class: {
        getContext: () => fake_context2d,
        getCenterCoords: () => { return {x:0, y:0}}
    }
};
const calledContextMock = fake_context2d.getCalledMock();

describe('crop view ', () => {

    it('sets canvas_class to false by default', () => {
        DRAW_CROP_VIEW.initOnce();
        const actual = DRAW_CROP_VIEW.test.getCanvas_class();
        expect(actual).toBe(false);
    });

    it('sets canvas_class to given object', () => {
        DRAW_CROP_VIEW.initOnce(fake_canvas_class);

        const actual = DRAW_CROP_VIEW.test.getCanvas_class();
        
        expect(actual).toEqual(fake_canvas_class.canvas_class);
    });

    it('sets content size', () => {
        DRAW_CROP_VIEW.setContentSize({width:300, height:400});
        const actual = {
            width: DRAW_CROP_VIEW.test.getContentWidth(),
            height: DRAW_CROP_VIEW.test.getContentHeight()
        };
        expect(actual).toEqual({width: 300, height: 400});
    });

    it('sets default content size', () => {
        DRAW_CROP_VIEW.initOnce();
        const actual = {
            width: DRAW_CROP_VIEW.test.getDefaultContentWidth(),
            height: DRAW_CROP_VIEW.test.getDefaultContentHeight()
        };
        expect(actual).toEqual({width: 400, height: 400});
    });

    it('calculates crop size', () => {
        const w = 100, h = 200;
        DRAW_CROP_VIEW.setContentSize({width:w, height:h});
        const cropRatio = DRAW_CROP_VIEW.test.getCropRatio();
        
        const actual = {
            width: DRAW_CROP_VIEW.test.getCropWidth(),
            height: DRAW_CROP_VIEW.test.getCropHeight()
        };

        expect(actual).toEqual({
            width: (w * cropRatio), 
            height: (h * cropRatio)
        });
    });

    it('draws stroke', () => {
        DRAW_CROP_VIEW.initOnce(fake_canvas_class);
        DRAW_CROP_VIEW.drawCropOutline();
        expect(calledContextMock).toBeCalledWith('stroke');
    });

});