const THREE = global.THREE = require('three');
const DEPTH_IMAGE = require('../../../pub/js/image');

describe('newMaterial ', () => {

    it('creates material with a displacement image path', () => {
        const material = DEPTH_IMAGE.newMaterial({
            path: 'some/path.png'
        });

        expect(material).toBeTruthy();
        expect(material.alphaMap).toBeTruthy();
    });

    it('creates material with a scale', () => {
        const material = DEPTH_IMAGE.newMaterial({
            path: 'some/path.png',
            scale: .4
        });

        expect(.4).toEqual(material.displacementScale);
    });

    it('says error when no path is provided', () => {
        const material = DEPTH_IMAGE.newMaterial();
        expect(material).toEqual('error - no displacement map path provided');
    });

});

describe('setScale ', () => {

    it('changes scale', () => {
        var material = DEPTH_IMAGE.newMaterial({
            path: 'some/path.png',
            scale: .4
        });

        DEPTH_IMAGE.setScale(.5);

        material = DEPTH_IMAGE.getMaterial();

        expect(.5).toEqual(material.displacementScale);
    });

});

describe('setPath ', () => {

    it('changes path', () => {
        var material = DEPTH_IMAGE.newMaterial({
            path: 'some/path.png',
            scale: .4
        });

        material = DEPTH_IMAGE.getMaterial();
        const firstImageUUID = material.displacementMap;

        DEPTH_IMAGE.setPath('new/path.png');

        const secondImageUUID = material.displacementMap;

        expect(firstImageUUID).toBeTruthy();
        expect(secondImageUUID).toBeTruthy();
        expect(firstImageUUID).not.toEqual(secondImageUUID);
    });

});