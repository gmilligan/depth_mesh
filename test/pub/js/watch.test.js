const DEPTH_WATCH = require('../../../pub/js/watch');

const examplePath1 = 'example/path/one.png';
const examplePath2 = 'example/path/two.png';
const exampleChanges = {
    "status":"ok",
    "changes":[
        {
            "path":examplePath1,
            "lastMod":"Tue Dec 24 2019 10:29:45 GMT-0500 (Eastern Standard Time)",
            "newMod":"Tue Dec 24 2019 10:29:45 GMT-0500 (Eastern Standard Time)"
        },
        {
            "path":examplePath2,
            "lastMod":"Wed Dec 25 2019 10:29:45 GMT-0500 (Eastern Standard Time)",
            "newMod":"Wed Dec 25 2019 10:29:45 GMT-0500 (Eastern Standard Time)"
        }
    ]};

beforeEach(() => {
    DEPTH_WATCH.setDependencies({
        ajaxPost: (routePath, callback, sendArgs) => {
            callback({
                status: 'ok', 
                path: sendArgs.path
            });
        },
        ajaxGet: (routePath, callback) => {
            callback({
                status: 'ok', 
                changes: [exampleChanges.changes[0]]
            });
        },
        clearTimeout: (t) => {},
        setTimeout: (callback, rate) => {
            callback();
        },
        requestAnimationFrame: (callback) => {}
    });
});

describe('newWatch ', () => {

    afterEach(() => {
        DEPTH_WATCH.unwatchAll();
    });

    it('creates one new watching item', () => {
        const result = DEPTH_WATCH.newWatch({
            path: examplePath1,
            change: (p) => {
                console.log('change! ' + p);
            }
        });

        const actualWatching = DEPTH_WATCH.test.getWatching();

        const expected = {}; expected[examplePath1] = {
            change: (p) => {
                console.log('change! ' + p);
            },
            path:examplePath1
        };

        expect(result).toEqual('ok');
        expect(JSON.stringify(expected))
            .toEqual(JSON.stringify(actualWatching));
        expect(Object.keys(expected[examplePath1]).sort())
            .toEqual(Object.keys(actualWatching[examplePath1]).sort());
        expect(expected[examplePath1].change.toString())
            .toEqual(actualWatching[examplePath1].change.toString());
    });

    it('calls change event for new watch item', () => {
        const calledMock = jest.fn();

        DEPTH_WATCH.newWatch({
            path: examplePath1,
            change: (p) => {
                calledMock();
            }
        });

        expect(calledMock).toBeCalled();
    });

    it('calls requestAnimationFrame to continue watching for changes', () => {
        const calledMock = jest.fn();

        DEPTH_WATCH.setDependencies({
            requestAnimationFrame: (callback) => {
                calledMock();
            }
        });

        DEPTH_WATCH.newWatch({
            path: examplePath1,
            change: (p) => {}
        });

        expect(calledMock).toBeCalled();
    });

    it('calls setTimeout to add delay to watch-ping', () => {
        const setTimeoutMock = jest.fn();

        DEPTH_WATCH.setDependencies({
            setTimeout: (callback) => {
                setTimeoutMock();
            }
        });

        DEPTH_WATCH.newWatch({
            path: examplePath1,
            change: (p) => {}
        });

        expect(setTimeoutMock).toBeCalled();
    });

    it('calls clearTimeout to clear timeout pile-up', () => {
        const clearTimeoutMock = jest.fn();

        DEPTH_WATCH.setDependencies({
            clearTimeout: (callback) => {
                clearTimeoutMock();
            }
        });

        DEPTH_WATCH.newWatch({
            path: examplePath1,
            change: (p) => {}
        });

        expect(clearTimeoutMock).toBeCalled();
    });

    it('calls watch recursively multiple times and only calls changes if they are returned by the ping watch', () => {
        const countFrameMock = jest.fn();
        const changeCallbackMock = jest.fn();

        DEPTH_WATCH.setDependencies({
            requestAnimationFrame: function(watch) {

                countFrameMock();

                if(watch.frameCount == undefined){
                    watch.frameCount = 1;
                }else{
                    watch.frameCount+=1;
                }

                if(watch.frameCount<3){
                    DEPTH_WATCH.setDependencies({
                        ajaxGet: (p, c) => {
                            c({
                                status: 'ok', 
                                changes: []
                            });
                        }
                    });

                    watch();
                }
            }
        });

        DEPTH_WATCH.newWatch({
            path: examplePath1,
            change: (p) => {
                changeCallbackMock();
            }
        });

        expect(changeCallbackMock).toHaveBeenCalledTimes(1); 
        expect(countFrameMock).toHaveBeenCalledTimes(3); 
    });

    it('is watching nothing, at the start', () => {
        const actualWatching = DEPTH_WATCH.test.getWatching();

        expect({}).toEqual(actualWatching);
        expect(DEPTH_WATCH.test.getIsWatchingFlag())
            .toBeFalsy();
    });

    it('says when duplicate watch requested', () => {
        DEPTH_WATCH.newWatch({
            path: examplePath1,
            change: (p)=>{}
        });
        const duplicateResult = DEPTH_WATCH.newWatch({
            path: examplePath1,
            change: (p)=>{}
        });
        expect(duplicateResult)
            .toEqual('error - already watching path');
    });

    it('says when no path is provided', () => {
        const result = DEPTH_WATCH.newWatch({});
        expect(result)
            .toEqual('error - no path provided');
    });

    it('says when no change event has been provided', () => {
        const result = DEPTH_WATCH.newWatch({
            path: examplePath1
        });
        expect(result)
            .toEqual('error - no change event provided');
    });
    
});

describe('unwatchAll ', () => {

    it('clears all watched items', () => {
        const result = DEPTH_WATCH.newWatch({
            path: examplePath1,
            change: (p) => {
                console.log('change! ' + p);
            }
        });

        DEPTH_WATCH.unwatchAll();

        const actualWatching = DEPTH_WATCH.test.getWatching();

        expect(result).toEqual('ok');
        expect({}).toEqual(actualWatching);
        expect(DEPTH_WATCH.test.getIsWatchingFlag())
            .toBeFalsy();
    });

});