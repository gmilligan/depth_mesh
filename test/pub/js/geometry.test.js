const THREE = global.THREE = require('three');
const DEPTH_GEOMETRY = require('../../../pub/js/geometry');

describe('newGeometry ', () => {

    it('creates plane geometry', () => {
        const material = new THREE.ShaderMaterial();

        const plane = DEPTH_GEOMETRY.newGeometry({
            material: material
        });

        expect(plane).toBeTruthy();
    });

});