const DEPTH_GEOMETRY = (()=>{

    var plane;

    const getArgs = (args) => {
        if(!args){ args={}; }
        if(!args.material){ args.material=undefined; }
        return args;
    };

    const newGeometry = (args) => {
        args = getArgs(args);
        geometry = new THREE.PlaneBufferGeometry( 1, 1, 111, 111 );
    
        plane = new THREE.Mesh(geometry, args.material);
        plane.rotation.x = - Math.PI * 0.5;

        plane.scale.x = 1.25;
        plane.scale.y = 1.25;
        plane.scale.z = 1.25;

        return plane;
    };

    return {
        newGeometry: (args) => newGeometry(args)
    };

})();

if (typeof exports !== 'undefined') { 
    for(var k in DEPTH_GEOMETRY) exports[k]=DEPTH_GEOMETRY[k];
}