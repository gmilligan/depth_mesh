const DEPTH_CANVAS = (()=>{

    var scene, camera, renderer, 
        ambientLight, hemisphereLight, canvas,
        controls, isMouseDown, isPersistentRedraw;

    const getArgs = (args) => {
        if(!args){ args={}; }
        if(!args.hasOwnProperty('persistentRedraw')){ args.persistentRedraw=true; }
        if(!args.hasOwnProperty('orbitControls')){ args.orbitControls=false; }
        return args;
    };

    const initOnce = (args) => {
        args = getArgs(args);
        isMouseDown = false;
        isPersistentRedraw = false;

        initScene();
        initCanvasElement();
        initCamera();
        setCamera();
        initLight();
        initOrbitControls(args);
        initEvents();

        if(args.persistentRedraw){
            persistentRedraw();
        }else{
            redraw();
        }
    };

    const initEvents = () => {
        canvas.addEventListener('mousedown', (e)=>{
            isMouseDown = true;
        });

        canvas.addEventListener('mouseup', (e)=>{
            isMouseDown = false
        });

        canvas.addEventListener('mouseleave', (e)=>{
            isMouseDown = false
        });
    };

    const initScene = () => {
        scene = new THREE.Scene();
        renderer = new THREE.WebGLRenderer({
            alpha: true, 
            antialias: true
        });
        renderer.setPixelRatio( window.devicePixelRatio );
        renderer.setSize( window.innerWidth, window.innerHeight );
    };
    
    const initCanvasElement = () => {
        canvas = renderer.domElement;
        document.body.appendChild(canvas);
    };

    const updateControls = () => {
        if(controls) {
            controls.update();
        }
    }

    const initCamera = () => {
        var fov = 40; // camera field-of-view in degrees
        var width = canvas.width;
        var height = canvas.height;
        var aspect = width / height; // view aspect ratio
        var near = .1;
        var far = 10000;
        camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
    };

    const setCamera = () => {
        camera.position.x = 0
        camera.position.y = 2;
        camera.position.z = 2;
    
        camera.lookAt(scene.position);

        updateControls();
    };

    const initLight = () => {
        ambientLight = new THREE.AmbientLight(0xffffff, 1);
        scene.add(ambientLight);

        hemisphereLight = new THREE.HemisphereLight(0xffffff, 0xffffff, .2);
        scene.add(hemisphereLight);
    };

    const resizeScene = (e) => {
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();
    
        renderer.setSize(window.innerWidth, window.innerHeight);

        redraw();
    };

    const redraw = () => {
        if(controls && (controls.enableDamping || controls.autoRotate)) {
            updateControls();
        }
        renderer.render(scene, camera);
    };

    const initOrbitControls = (args) => {
        if(args.orbitControls){
            controls = new THREE.OrbitControls(camera, canvas);

            if(!args.persistentRedraw) {
                redrawWhileOrbit();
            }
        }
    };

    const redrawWhileOrbit = () => {
        if(!isPersistentRedraw){
            requestAnimationFrame( redrawWhileOrbit );
            if(isMouseDown){
                redraw();
            }
        }
    };

    const persistentRedraw = () => {
        isPersistentRedraw = true;
        requestAnimationFrame( persistentRedraw );
        redraw();
    };

    const add = (obj) => {
        if(scene){
            scene.add(obj);
        }
    };

    const remove = (obj) => {
        if(scene){
            if(obj){
                scene.remove(obj.name);
            }
        }
    };

    return {
        initOnce: (args) => { initOnce(args); },
        setCamera: () => { setCamera(); },
        resizeScene: () => { resizeScene(); },
        persistentRedraw: () => { persistentRedraw(); },
        redraw: () => { redraw(); },
        add: (obj) => { add(obj); },
        remove: (obj) => { remove(obj); },
        //getRenderer: () => renderer,
        //getCamera: () => camera,
        //getScene: () => scene
    };

})();

if (typeof exports !== 'undefined') { 
    for(var k in DEPTH_CANVAS) exports[k]=DEPTH_CANVAS[k];
}