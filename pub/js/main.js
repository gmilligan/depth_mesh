const DEPTH_MAIN = (()=>{

  var material, plane;

  const initOnce = () => {

    DEPTH_CANVAS.initOnce({
      persistentRedraw: false,
      orbitControls: true
    });

    DEPTH_WATCH.setDependencies({
      ajaxGet, ajaxPost
    });

  };

  const setImagePath = (path) => {

    // SHADERS
    material = DEPTH_IMAGE.newMaterial({
      path: path,
      scale: .3,
      load: () => { //load happens when an image path is set/loaded
        DEPTH_CANVAS.redraw();
      }
    });

    // GEOMETRY
    plane = DEPTH_GEOMETRY.newGeometry({
      material: material
    });

    DEPTH_CANVAS.add(plane);

    //INIT WATCHING FOR IMAGE FILE CHANGES
    DEPTH_WATCH.newWatch({
      path: path,
      change: (path) => {
        DEPTH_IMAGE.setPath(path);
      }
    });

  };

  return {
    initOnce: () => { initOnce(); },
    setImagePath: (path) => { setImagePath(path); }
  }

})();

window.addEventListener('resize', (e) => {
    DEPTH_CANVAS.resizeScene(e); 
});

document.addEventListener('DOMContentLoaded', (e) => {

    DEPTH_MAIN.initOnce();
    DEPTH_MAIN.setImagePath('img/eskimo.png');

    
    //HOW TO UPDATE
    // setTimeout(()=>{
    //   DEPTH_IMAGE.setScale(1);
    //   DEPTH_CANVAS.redraw();

    //   DEPTH_IMAGE.setPath('img/US_heightmap_500px.png');
      // DEPTH_MAIN.setImagePath('img/US_heightmap_500px.png');
    // }, 1000);
    
});