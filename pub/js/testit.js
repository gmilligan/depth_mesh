const DEPTH_TEST = (()=>{

    const hello = (msg) => msg + ' back';

    return {
        test: {
            hello: (msg) => hello(msg)
        }
    }

})();

if (typeof exports !== 'undefined') { 
    for(var k in DEPTH_TEST) exports[k]=DEPTH_TEST[k];
}