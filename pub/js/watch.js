const DEPTH_WATCH = (()=>{

    const pingRate = 333;

    var watching = {}, isWatching = false, pingTimeout,
    aPost, aGet, timeout, clearTime, nextFrame;

    const setDependencies = (args) => {
        if (args.hasOwnProperty('ajaxPost')) {
            aPost = args.ajaxPost;
        }

        if (args.hasOwnProperty('ajaxGet')) {
            aGet = args.ajaxGet;
        }

        if(args.hasOwnProperty('clearTimeout')) {
            clearTime = args.clearTimeout;
        }else if(!clearTime){
            clearTime = clearTimeout;
        }

        if(args.hasOwnProperty('setTimeout')) {
            timeout = args.setTimeout;
        }else if(!timeout){
            timeout = setTimeout;
        }

        if(args.hasOwnProperty('requestAnimationFrame')) {
            nextFrame = args.requestAnimationFrame;
        }else if(!nextFrame){
            nextFrame = requestAnimationFrame;
        }
    };

    const hasPath = (args) => args.hasOwnProperty('path') && args.path.length > 0;
    const isNewPath = (args) => !watching.hasOwnProperty(args.path);
    const hasChange = (args) => args.hasOwnProperty('change') && typeof args.change == 'function';

    const getPathStatus = (args) => {
        args = getWatchArgs(args);
        if(!isNewPath(args)) { return 'error - already watching path'; }
        if(!hasPath(args)){ return 'error - no path provided'; }
        if(!hasChange(args)){ return 'error - no change event provided'; }
        return 'ok';
    }

    const getWatchArgs = (args) => {
        if(!args){ args={}; }
        if(!args.path){ args.path=''; }
        if(!args.change){ args.change=''; }
        return args;
    };

    const newWatch = (args) => {
        const status = getPathStatus(args);
        if(status != 'ok'){ return status; }
            
        aPost('/newwatch', (res)=>{
            if(res.status==='ok'){
                watching[res.path] = {
                    path: res.path,
                    change: args.change
                };
                console.log('new watch - ' + res.path);
                if(!isWatching){
                    isWatching = true;
                    watch();
                }
            }else{
                console.log('new watch failed - ' + res.status);
            }
        },{
            path: args.path, 
            isWatching: isWatching
        });

        return 'ok';
    };

    const pingDelay = (callback) => {
        clearTime(pingTimeout);
        pingTimeout=timeout(callback, pingRate);
    };

    const watch = () => {
        pingDelay(()=>{
            if(isWatching) {
                aGet('/pingwatch', (res)=>{
                    if(res.status==='ok'){
                        changeCalls(res.changes);
                    }else{
                        console.log('ping watch failed - ' + res.status);
                    }
                    nextFrame(watch);
                });
            }
        });
    };

    const unwatchAll = () => {
        Object.keys(watching).forEach(path => {
            console.log('unwatch - ' + path);
        });
        watching = {};
        isWatching = false;
    };

    const changeCalls = (changes) => {
        changes.forEach((watched)=>{
            console.log('change detected - ' + watched.path);
            watching[watched.path].change(watched.path);
        });
    };

    return {
        setDependencies: (args) => { setDependencies(args); },
        newWatch: (args) => newWatch(args),
        unwatchAll: () => { unwatchAll(); },
        test: {
            getWatching: () => watching,
            getIsWatchingFlag: () => isWatching
        }
    };

})();

if (typeof exports !== 'undefined') { 
    for(var k in DEPTH_WATCH) exports[k]=DEPTH_WATCH[k];
}