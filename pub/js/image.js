const DEPTH_IMAGE = (()=>{

    var material, myTexture, loader, pathCallback;

    const getArgs = (args) => {
        if(!args){ args={}; }
        if(!args.path){ args.path=''; }
        if(!args.scale){ args.scale=.3; }
        if(!args.load){ args.load=(path)=>{ console.log('loaded: ' + path); }; }
        return args;
    };

    const hasPath = (args) => args.hasOwnProperty('path') && args.path.length > 0;

    const newMaterial = (args) => {
        args = getArgs(args);

        if(!hasPath(args)) { return 'error - no displacement map path provided'; }

        pathCallback = args.load;

        material = new THREE.MeshPhongMaterial({ 
			wireframe: true,
			alphaTest: 0.5 // discard fragments with certain alpha values
        });

        setScale(args.scale);
        setPath(args.path);

        return material;
    };

    const setScale = (scale) => {
        if(material){
            material['displacementScale'] = scale;
        }
    };

    const setPath = (path) => {
        if(material){

            if(!loader) { 
                loader = new THREE.TextureLoader();
            }

            if(myTexture){
                myTexture.dispose();
            }

            myTexture = loader.load(
                path+'?nocache='+new Date().getTime().toString(), 
                (texture)=>{
                    if(pathCallback){
                        pathCallback(path);
                    }
                },(err)=>{
                    console.error('error - failed to load texture: ' + path);
                });

            // improve the texture filter to avoid displacement spike noise
            myTexture.minFilter = THREE.NearestFilter;
            myTexture.magFilter = THREE.NearestFilter;

            material['displacementMap'] = myTexture;
            material['alphaMap'] = myTexture;
        }
    };

    return {
        newMaterial: (args) => newMaterial(args),
        getMaterial: () => material,
        setScale: (scale) => setScale(scale),
        setPath: (path, callback) => setPath(path, callback)
    };

})();

if (typeof exports !== 'undefined') { 
    for(var k in DEPTH_IMAGE) exports[k]=DEPTH_IMAGE[k];
}