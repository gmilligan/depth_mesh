const svgNs = 'http://www.w3.org/2000/svg',
    initialHueIndex = 17,
    grayHues = [
        '#000000', '#080808', '#101010', '#181818', '#202020', 
        '#282828', '#303030', '#383838', '#404040', '#484848', 
        '#505050', '#585858', '#606060', '#686868', '#696969', 
        '#707070', '#787878', '#808080', '#888888', '#909090', 
        '#989898', '#A0A0A0', '#A8A8A8', '#A9A9A9', '#B0B0B0', 
        '#B8B8B8', '#BEBEBE', '#C0C0C0', '#C8C8C8', '#D0D0D0', 
        '#D3D3D3', '#D8D8D8', '#DCDCDC', '#E0E0E0', '#E8E8E8', 
        '#F0F0F0', '#F5F5F5', '#F8F8F8', '#FFFFFF'
    ];

var prevMousedownCoords, exportTimeout;

const initOnce = () => {
    prevMousedownCoords = false;
    updateCropBox();
};

const getMainEl = () => document.getElementById('main');
const getSvgAreaEl = () => document.getElementById('svg_area');
const getMirrorXBtnEl = () => document.getElementById('btn_mirror_x');
const getMirrorXAreaEl = () => document.getElementById('mirror_x_area');
const getSquareCropBtnEl = () => document.getElementById('btn_square_crop');
const getCropXInputEl = () => document.getElementById('crop_x');
const getCropYInputEl = () => document.getElementById('crop_y');
const getCropHandleEl = (which) => getSvgAreaEl().querySelector(`.${which}_crop .handle`);
const getCropHandleDownEl = () => getSvgAreaEl().querySelector('.crop .crop-handle-down');
const getWhichCropHandle = (cropHandleEl) => {
    const cropClasses = [...cropHandleEl.parentElement.classList].filter(cl => cl.includes('_crop'));
    return cropClasses.length > 0 ? cropClasses[0].substring(0, cropClasses[0].indexOf('_')).toLowerCase() : '';
};
const getCropHandleEls = () => {
    return {
        left: getCropHandleEl('left'),
        top: getCropHandleEl('top'),
        right: getCropHandleEl('right'),
        bottom: getCropHandleEl('bottom')
    };
};
const getCropXValues = (leftEl, rightEl) => {
    const size = rightEl.getBoundingClientRect().left - leftEl.getBoundingClientRect().right;
    const sizeInt = Math.round(size);
    return {
        int: sizeInt,
        double: size
    };
};
const getCropYValues = (topEl, bottomEl) => {
    const size = bottomEl.getBoundingClientRect().top - topEl.getBoundingClientRect().bottom;
    const sizeInt = Math.round(size);
    return {
        int: sizeInt,
        double: size
    };
};
const getCropBoxEls = () => {
    const svgAreaEl = getSvgAreaEl();
    const leftEl = svgAreaEl.querySelector('.left_crop');
    const topEl = svgAreaEl.querySelector('.top_crop');
    const rightEl = svgAreaEl.querySelector('.right_crop');
    const bottomEl = svgAreaEl.querySelector('.bottom_crop');
    return {
        left: leftEl,
        top: topEl,
        right: rightEl,
        bottom: bottomEl,
        inputX: getCropXInputEl(),
        inputY: getCropYInputEl(),
        x: getCropXValues(leftEl, rightEl),
        y: getCropYValues(topEl, bottomEl)
    };
};
const getLayersAreaEl = () => document.getElementById('layers_area');
const getSvgEl = () => getSvgAreaEl().querySelector('svg#draw_svg');
const getSvgMirrorXEl = () => getMirrorXAreaEl().querySelector('svg#mirror_x_svg');
const getSelectedGroupEl = () => getSvgEl().querySelector('g.selected');
const getMousedownEl = () => getSvgEl().querySelector('g .mousedown');
const hasEnableMirrorX = () => document.body.classList.contains('mirror-x');
const hasEnableSquareCrop = () => document.body.classList.contains('square-crop');
const getColorSliderHandleDownEl = () => document.querySelector('#color-slider .handle-down');
const getColorSliderHandleEl = () => document.querySelector('#color-slider .handle');
const getColorSliderInputEl = () => document.querySelector('#color-slider .text input');
const getColorSliderNameEl = () => document.querySelector('#color-slider .name');
const getColorSliderHexEl = () => document.querySelector('#color-slider .text .hex');
const getElIndex = (el, selector) => {
    if(!el){return -1;}
    var index = 0, retIndex = -1;
    el.classList.add('findIndex');
    el.parentElement.querySelectorAll(selector).forEach(e => {
        if(e.classList.contains('findIndex')) {
            e.classList.remove('findIndex');
            retIndex = index;
            return index;
        }
        index++;
    });
    return retIndex;
};
const removeOutputCanvas = () => {
    const canvas = document.getElementById('canvas');
    if(canvas){
        canvas.parentNode.removeChild(canvas);
    }
};
const getCreateOutputCanvas = (width, height) => {
    removeOutputCanvas();

    var canvas = document.createElement('canvas');
    canvas.setAttribute('id', 'canvas');
    canvas.setAttribute('width', width);
    canvas.setAttribute('height', height);
    document.body.appendChild(canvas);
    return canvas;
};
const getSvgString = (svgEl) => new XMLSerializer().serializeToString(svgEl);
const loadSvgAsImage = (svgEl, loadCallback) => {
    const domurl = self.URL || self.webkitURL || self;
    const svg = new Blob([getSvgString(svgEl)], {type: 'image/svg+xml;charset=utf-8'});
    const img = new Image();

    img.onload = () => {
        loadCallback(img);
        domurl.revokeObjectURL(img.src);
    };
    
    img.src = domurl.createObjectURL(svg);
};
const loadCanvasAsImage = (canvas, loadCallback) => {
    const url = canvas.toDataURL('image/png');
    const img = new Image();
    img.onload = () => {
        loadCallback(img);
    };
    img.src = url;
};
const loadCroppedImage = (svgEl, captureAtX, captureAtY, captureWidth, captureHeight, loadCallback) => {
    const fullCanvas = getCreateOutputCanvas(svgEl.clientWidth, svgEl.clientHeight);
    const fullCtx = fullCanvas.getContext('2d');
    loadSvgAsImage(svgEl, (img) => {
        fullCtx.drawImage(img, 0, 0, svgEl.clientWidth, svgEl.clientHeight);
        loadCanvasAsImage(fullCanvas, (fullImage) => {
            const cropCanvas = getCreateOutputCanvas(captureWidth, captureHeight);
            const cropCtx = cropCanvas.getContext('2d');
            cropCtx.drawImage(fullImage, 
                captureAtX, captureAtY, // capture image at
                captureWidth, captureHeight, // capture size
                0, 0, // move paste to
                captureWidth, captureHeight // stretch to capture size
            );
            loadCanvasAsImage(cropCanvas, (cropImage) => {
                loadCallback(cropImage);
            });
        });
    });
};

const loadMirroredImage = (box, rightOrLeft, loadCallback) => {
    const isRight = () => rightOrLeft === 'right';
    const svgEl = isRight() ? getSvgEl() : getSvgMirrorXEl();
    const captureX = isRight() ? 0 : box.left.clientWidth;

    const captureY = box.top.clientHeight;
    const captureWidth = box.x.int / 2;
    const captureHeight = box.y.int;

    loadCroppedImage(
        svgEl, captureX, captureY, captureWidth, captureHeight,
        (img) => {
            loadCallback(img);
        }
    );
};

const loadMirroredImages = (box, loadCallback) => {
    loadMirroredImage(box, 'right', (rightImg) => {
        loadMirroredImage(box, 'left', (leftImg) => {
            const canvas = getCreateOutputCanvas(box.x.int, box.y.int);
            const ctx = canvas.getContext('2d');
            ctx.drawImage(leftImg, 
                0, 0, box.x.int / 2, box.y.int);
            ctx.drawImage(rightImg, 
                box.x.int / 2, 0, box.x.int / 2, box.y.int);
            loadCanvasAsImage(canvas, (img) => {
                loadCallback(img);
            });
        });
    });
};

const loadMainImage = (box, loadCallback) => {
    loadCroppedImage(
        getSvgEl(), 
        box.left.clientWidth, box.top.clientHeight, 
        box.x.int, box.y.int,
        (img) => {
            loadCallback(img);
        }
    );
};
const writeAsImage = (img, writeCallback) => {
    ajaxPost('/saveimage', (res)=>{
        if(res.status==='ok'){
            console.log('written - ' + res.path);
            if(writeCallback) { 
                writeCallback(res); }
        }else{
            console.log('image-write failed - ' + res.status); }
    },{
        img:img.src, path:'img/eskimo.png'
    });
};
const exportAsImage = (beforeExport, afterExport) => {
    clearTimeout(exportTimeout);
    exportTimeout = setTimeout(() => {

        const box = getCropBoxEls();
        if(hasEnableMirrorX()) {

            loadMirroredImages(box, (img) => {
                writeAsImage(img, afterExport);
                if(beforeExport) { 
                    beforeExport(img); }
            });

        }else{

            loadMainImage(box, (img) => {
                writeAsImage(img, afterExport);
                if(beforeExport) { 
                    beforeExport(img); }
            });
        }

    }, 100);
};
const getGroupIndex = (gEl) => getElIndex(gEl, 'g');
const getLayerIndex = (layerEl) => getElIndex(layerEl, 'div.layer');
const getMaxGroupIndex = () => getSvgEl().querySelectorAll('g').length - 1;
const getGroupElByIndex = (index) => getSvgEl().querySelectorAll('g')[index];
const getLayerElByIndex = (index) => getLayersAreaEl().querySelectorAll('div.layer')[index];
const getLayerElByGroupEl = (gEl) => {
    const index = getGroupIndex(gEl);
    return getLayerElByIndex(index);
};
const getGroupElByLayerEl = (layerEl) => {
    const index = getLayerIndex(layerEl);
    return getGroupElByIndex(index);
};
const getSelectedGroupElIndex = () => {
    const gEl = getSelectedGroupEl();
    return getGroupIndex(gEl);
};
const getPointX = (circle) => parseFloat(circle.attributes['cx'].value);
const getPointY = (circle) => parseFloat(circle.attributes['cy'].value);
const getGroupPoints = (gEl) => {
    let points = [];
    gEl.querySelectorAll('circle').forEach(circle => {
        points.push({
            x: getPointX(circle),
            y: getPointY(circle),
            circleEl: circle
        });
    });
    return points;
};
const getGroupData = (gEl) => {
    gEl = gEl ? gEl : getGroupEl();

    if(!gEl) { return undefined; }

    const hex = getGroupHueHex(gEl);
    const hueIndex = grayHues.indexOf(hex);
    const points = getGroupPoints(gEl);

    var polygon = {
        gEl: gEl,
        fill: {
            hex: hex,
            index: hueIndex
        },
        points: points
    };

    return polygon;
};
const getGroupsData = () => {
    const svgEl = getSvgEl();
    var polygons = [];
    svgEl.querySelectorAll('g').forEach(gEl => {
        const polygon = getGroupData(gEl);
        polygons.push(polygon);
    });
    return polygons;
};
const getPolyIndex = (polyIndex) => polyIndex ? polyIndex : getSelectedGroupElIndex();
const getGroupEl = (polyIndex) => {
    polyIndex = getPolyIndex(polyIndex);
    return getGroupElByIndex(polyIndex);
};

const setEditDrawMode = () => {
    document.body.classList.add('edit-draw');
    document.body.classList.remove('add-draw');
};

const setAddDrawMode = () => {
    document.body.classList.remove('edit-draw');
    document.body.classList.add('add-draw');
};

const deselectGroups = (e) => {
    stopDefault(e);
    const svgEl = getSvgEl();
    svgEl.querySelectorAll('g.selected').forEach(g => {
        g.classList.remove('selected');

        var layerEl = getLayerElByGroupEl(g);
        layerEl.classList.remove('selected');
    });
    setAddDrawMode();
};

const selectGroup = (gEl) => {
    if(gEl) {
        if(!gEl.classList.contains('selected')) {
            const svgEl = getSvgEl();
            svgEl.querySelectorAll('g.selected').forEach(g => {
                g.classList.remove('selected');
            });
            gEl.classList.add('selected');

            const layerEl = getLayerElByGroupEl(gEl);
            layerEl.parentElement.querySelectorAll('div.layer').forEach(layer => {
                layer.classList.remove('selected');
            });
            layerEl.classList.add('selected');

            setEditDrawMode();
        }
    }
};

const getGroupHueHex = (gEl) => {
    const polygon = gEl.querySelector('polygon');
    if(polygon) {
        const rgb = polygon.style.fill;
        const vals = rgb.indexOf('rgb')===0 ? rgb.substring(rgb.indexOf('(') + '('.length) : rgb;
        if(vals.includes(',')){
            const nums = vals.split(',').map(v => parseFloat(v));
            const toHex = (c) => {
                var hex = c.toString(16);
                return hex.length == 1 ? "0" + hex : hex;
            };
            return ("#" + toHex(nums[0]) + toHex(nums[1]) + toHex(nums[2])).toUpperCase();
        }
        return vals;
    }
    return grayHues[initialHueIndex];
};

const colorSliderIsOpen = (colorSliderDiv) => {
    const div = colorSliderDiv ? colorSliderDiv : getOrCreateColorSliderEl();
    return div.classList.contains('open');
};

const closeColorSlider = (e) => {
    const colorSliderDiv = getOrCreateColorSliderEl();
    if(colorSliderIsOpen(colorSliderDiv)) {
        stopDefault(e);
        colorSliderDiv.classList.remove('open');
        return true;
    }
    return false;
};

const openColorSliderAtCursor = (gEl, e) => {
    selectGroup(gEl);

    const gBounds = gEl.getBoundingClientRect();
    e = e ? e : {clientX: gBounds.x, clientY: gBounds.y};

    const colorSliderDiv = getOrCreateColorSliderEl();
    colorSliderDiv.classList.add('open');

    const groupIndex = getGroupIndex(gEl);
    colorSliderDiv.setAttribute('data-group-index', groupIndex);

    const hex = getGroupHueHex(gEl);
    const hueIndex = grayHues.indexOf(hex);
    setColorSliderHughIndex(hueIndex);
    setColorInputHughIndex(hueIndex);
    setColorHexHughIndex(hueIndex);

    const groupName = gEl.attributes['data-name'].value;
    const sliderName = getColorSliderNameEl();
    sliderName.innerHTML = groupName;

    const padWidth = 20;    

    var newLeft = e.clientX;
    const newRight = colorSliderDiv.clientWidth + e.clientX;
    if(newRight > window.innerWidth + padWidth) {
        const diffX = newRight - window.innerWidth;
        newLeft -= diffX + padWidth;
    }
    colorSliderDiv.style.left = newLeft + 'px';

    var newTop = e.clientY;
    const newBottom = colorSliderDiv.clientHeight + e.clientY;
    if(newBottom > window.innerHeight + padWidth) {
        const diffY = newBottom - window.innerHeight;
        newTop -= diffY + padWidth;
    }
    colorSliderDiv.style.top = newTop + 'px';

    const colorInput = getColorSliderInputEl();
    colorInput.focus();
};

const createNewLayer = (gEl) => {
    const layersAreaEl = getLayersAreaEl();

    var layerEl = document.createElement('div');
    layerEl.setAttribute('class', 'layer');

    layersAreaEl.appendChild(layerEl);

    layerEl.addEventListener('click', (e) => {
        const g = getGroupElByLayerEl(e.target);
        selectGroup(g);
    });

    const setLayerContents = () => {
        const setLayerName = () => {
            var inputNameEl = document.createElement('input');
            inputNameEl.classList.add('name');
            inputNameEl.setAttribute('value', gEl.attributes['data-name'].value);
            layerEl.appendChild(inputNameEl);

            inputNameEl.addEventListener('focus', (e) => {
                const g = getGroupElByLayerEl(e.target.parentElement);
                selectGroup(g);
            });
    
            inputNameEl.addEventListener('blur', (e) => {
                const gEl = getGroupElByLayerEl(e.target.parentElement);
                if(e.target.value.trim().length > 0){
                    gEl.setAttribute('data-name', e.target.value);
                }else{
                    e.target.value = gEl.attributes['data-name'].value;
                }
            });
        }; setLayerName();

        const setColorPicker = () => {
            var colorDivEl = document.createElement('div');
            colorDivEl.classList.add('hue');
            colorDivEl.style.backgroundColor = grayHues[initialHueIndex];
            layerEl.appendChild(colorDivEl);
            
            colorDivEl.addEventListener('click', (e) => {
                stopDefault(e);
                openColorSliderAtCursor(getGroupElByLayerEl(e.target.parentElement), e);
            });
        }; setColorPicker();

    }; setLayerContents();
};

const getOrCreateColorSliderEl = () => {
    var colorSliderDiv = document.body.querySelector('div#color-slider');
    if(!colorSliderDiv) {
        colorSliderDiv = document.createElement('div');
        colorSliderDiv.setAttribute('id', 'color-slider');
        colorSliderDiv.innerHTML = `
<div class="name">Polygon</div>
<div class="slider">
    <div class="track">
        <div class="handle"><div class="needle"></div></div>
    </div>
</div>
<div class="text">
    <div class="hex">Color</div>
    <input type="number" min="0" max="38" value="7" />
</div>
        `;
        document.body.appendChild(colorSliderDiv);

        const sliderDiv = colorSliderDiv.querySelector('.slider');
        const handleDiv = sliderDiv.querySelector('.track .handle');
        const numberInput = colorSliderDiv.querySelector('.text input');

        handleDiv.addEventListener('mousedown', (e) => {
            startColorSliding(e);
        });
        sliderDiv.addEventListener('click', (e) => {
            stopDefault(e);
            alignColorSliderWithMouse(e, handleDiv);

            updateForChanges();
        });
        colorSliderDiv.addEventListener('selectstart', (e) => {
            stopDefault(e);
        });
        numberInput.addEventListener('change', (e) => {
            const hueIndex = parseInt(e.target.value);
            setColorSliderHughIndex(hueIndex);
            setColorHexHughIndex(hueIndex);
            setGroupHughByIndex(hueIndex);

            updateForChanges();
        });
    }
    return colorSliderDiv;
};

const createGroupEl = () => getOrCreateGroupEl(-1);

const getOrCreateGroupEl = (polyIndex) => {
    var gEl = polyIndex < 0 ? undefined : getGroupEl(polyIndex);
    if(!gEl) {
        
        const groupName = newGroupName();

        const svgEl = getSvgEl();
        gEl = document.createElementNS(svgNs, 'g');
        gEl.setAttributeNS(null, 'data-name', groupName);
        svgEl.appendChild(gEl);

        createNewLayer(gEl);

        selectGroup(gEl);
    }
    return gEl;
};

const deleteLayer = (gEl) => {
    const layerEl = getLayerElByGroupEl(gEl);
    gEl.parentNode.removeChild(gEl);
    layerEl.parentNode.removeChild(layerEl);
    closeColorSlider();
    deselectGroups();
    updateForChanges();
};

const getFocusEl = () => getSvgEl().querySelector('.focus');

const removeAll = (parentEl, selector) => {
    parentEl.querySelectorAll(selector).forEach(el => {
        el.parentNode.removeChild(el);
    });
};

const attachDynamicClassEvents = (el) => {
    el.addEventListener('mouseleave', (e) => {
        el.classList.remove('focus');
    });
    el.addEventListener('mouseenter', (e) => {
        el.classList.add('focus');
    });
    el.addEventListener('mousedown', (e) => {
        el.classList.add('mousedown');
        const svgEl = getSvgEl();
        prevMousedownCoords = getClickLocationOnElement(e, svgEl);
    });
    el.addEventListener('contextmenu', (e) => {
        stopDefault(e);
        selectGroup(e.target.parentElement);
    });
};

const preventClickEvents = (el) => {
    el.addEventListener('click', (e) => { 
        closeColorSlider(e); 
        if(e.target.parentElement.classList.contains('selected')) {
            stopDefault(e); 
        }
    });
};

const updatePolyLine = (polyIndex) => {
    const gEl = getOrCreateGroupEl(polyIndex);
    const polyData = getGroupData(gEl);
    removeAll(gEl, 'polygon');

    if(polyData.points.length > 1) {
        var polygon = document.createElementNS(svgNs, 'polygon');
        polygon.style.fill = polyData.fill.hex;
        var pointsString = '';
        polyData.points.forEach(point => {
            pointsString+=`${point.x},${point.y} `;
        });
        pointsString = pointsString.trim();
        polygon.setAttributeNS(null, 'points', pointsString);
        gEl.prepend(polygon);

        attachDynamicClassEvents(polygon);
        preventClickEvents(polygon);
    }

    updateForChanges();
};

const newGroupName = () => {
    const svgEl = getSvgEl();
    const gEls = svgEl.querySelectorAll('g');
    var newName = '0-Polygon';
    for(var i = 0; i < gEls.length; i++) {
        if(!svgEl.querySelector(`g[data-name="${newName}"]`)) {
            break;
        }else{
            newName = `${i+1}-Polygon`;
        }
    }
    return newName;
};

const getCenterPointForPoints = (gEl) => {
    const points = getGroupPoints(gEl);
    const xSum = points.map(point => point.x).reduce((x1, x2) => x1 + x2);
    const ySum = points.map(point => point.y).reduce((y1, y2) => y1 + y2);
    return {
        x: xSum / points.length,
        y: ySum / points.length,
        fromPoints: points
    };
};

const scalePointsAroundOrigin = (gEl, origin, scale) => {
    const points = origin.hasOwnProperty('fromPoints') ? origin.fromPoints : getGroupPoints(gEl);

    const scaleEquasion = (point, center) => scale * (point - center) + center;

    const scaledPoints = points.map(point => {
        const x = scaleEquasion(point.x, origin.x);
        const y = scaleEquasion(point.y, origin.y);

        point.circleEl.setAttribute('cx', x);
        point.circleEl.setAttribute('cy', y);

        return {x, y, circleEl: point.circleEl};
    });

    updatePolyLine();

    return scaledPoints;
};

const getDistanceBetweenPointAndLine = (point, line) => {
    var A = point.x - line.x1;
    var B = point.y - line.y1;
    var C = line.x2 - line.x1;
    var D = line.y2 - line.y1;

    var dot = A * C + B * D;
    var len_sq = C * C + D * D;
    var param = -1;
    if (len_sq != 0) //in case of 0 length line
        param = dot / len_sq;

    var xx, yy;

    if (param < 0) {
        xx = line.x1;
        yy = line.y1;
    }
    else if (param > 1) {
        xx = line.x2;
        yy = line.y2;
    }
    else {
        xx = line.x1 + param * C;
        yy = line.y1 + param * D;
    }

    var dx = point.x - xx;
    var dy = point.y - yy;
    return Math.sqrt(dx * dx + dy * dy);
};

const appendPointAtClosestLocation = (gEl, circle) => {
    const existingCircles = gEl.querySelectorAll('circle');
    const pointsCount = existingCircles.length;
    if(pointsCount < 3) {
        gEl.appendChild(circle);
    }else{
        const point = {
            x: getPointX(circle),
            y: getPointY(circle)
        };
        var shortestDistance = 9999, insertBeforeThisPoint;
        existingCircles.forEach((existingCircle, index) => {
            const nextPoint = index + 1 < existingCircles.length ? existingCircles[index + 1] : existingCircles[0];
            const line = {
                x1: getPointX(existingCircle), y1: getPointY(existingCircle),
                x2: getPointX(nextPoint), y2: getPointY(nextPoint)
            };
            const distance = getDistanceBetweenPointAndLine(point, line);
            if(distance < shortestDistance) {
                shortestDistance = distance;
                insertBeforeThisPoint = nextPoint;
            }
        });
        gEl.insertBefore(circle, insertBeforeThisPoint);
    }
};

const createCircle = (x, y) => {
    let circle = document.createElementNS(svgNs, 'circle');
    circle.setAttributeNS(null, 'cx', x);
    circle.setAttributeNS(null, 'cy', y);
    circle.setAttributeNS(null, 'r', 0);
    return circle;
};

const addPoint = (x, y) => {
    let circle = createCircle(x, y);

    const gEl = getOrCreateGroupEl();

    appendPointAtClosestLocation(gEl, circle);

    attachDynamicClassEvents(circle);
    preventClickEvents(circle);

    updatePolyLine();
};

const copyPointsToGroup = (fromGroup, toGroup) => {
    removeAll(toGroup, 'circle');
    selectGroup(toGroup);

    fromGroup.querySelectorAll('circle').forEach(circle => {
        const point = {
            x: getPointX(circle),
            y: getPointY(circle)
        };
        let newCircle = createCircle(point.x, point.y);
        toGroup.appendChild(newCircle);

        attachDynamicClassEvents(newCircle);
        preventClickEvents(newCircle);
    });

    updatePolyLine();
};

const createCloneOfGroup = (gElToCopy, beforeOrAfter) => { 
    gElToCopy = gElToCopy ? gElToCopy : getSelectedGroupEl();
    if(!gElToCopy) { return undefined; }
    beforeOrAfter = beforeOrAfter ? beforeOrAfter : 'after';
    const newGroupEl = createGroupEl();
    copyPointsToGroup(gElToCopy, newGroupEl);

    // TODO move newGroupEl before... if beforeOrAfter == 'before'

    return newGroupEl;
}

const duplicatePolyAroundSelected = () => {
    const gEl = getSelectedGroupEl();
    if(gEl){
        const gElCopied = createCloneOfGroup(gEl);

        const centerPoint = getCenterPointForPoints(gElCopied);
        scalePointsAroundOrigin(gElCopied, centerPoint, .7);

        // TODO
    }
};

const getCoordsDifference = (xy1, xy2) => {
    return {
        x: (xy1.x - xy2.x),
        y: (xy1.y - xy2.y)
    };
};

const getClickLocationOnElement = (e, el) => {
    const elBounds = el.getBoundingClientRect();
    return {
        x: e.clientX - elBounds.left,
        y: e.clientY - elBounds.top
    };
};

const deletePoint = (circleEl) => {
    const gEl = circleEl.parentNode;
    if(gEl.querySelectorAll('circle').length > 1) {
        gEl.removeChild(circleEl);

        const layerIndex = getGroupIndex(gEl);
        updatePolyLine(layerIndex);
    }else{
        deleteLayer(gEl);
    }
}; 

const deleteFocused = (e) => {
    const focusEl = getFocusEl();
    if(focusEl) {
        stopDefault(e);
        switch(focusEl.nodeName.toLowerCase()) {
            case 'polygon': deleteLayer(focusEl.parentElement); break;
            case 'circle': deletePoint(focusEl); break;
        }
        return true;
    }
    return false;
};

const stopDefault = (e) => { 
    if(e){
        e.preventDefault(); e.stopPropagation(); 
    }
};

const moveLayerGroupToIndex = (gEl, newIndex) => {
    newIndex = newIndex > 0 ? newIndex : 0;
    
    const maxGroupIndex = getMaxGroupIndex();
    newIndex = newIndex <= maxGroupIndex  ? newIndex : maxGroupIndex;

    const currentIndex = getGroupIndex(gEl);
    if(newIndex != currentIndex) {
        const layerEl = getLayerElByGroupEl(gEl);

        if(newIndex == maxGroupIndex) {
            gEl.parentElement.appendChild(gEl);
            layerEl.parentElement.appendChild(layerEl);
        }else{
            const beforeIndex = currentIndex < newIndex ? newIndex+1 : newIndex;

            const layerElBefore = getLayerElByIndex(beforeIndex);
            const gElBefore = getGroupElByIndex(beforeIndex);

            gEl.parentElement.insertBefore(gEl, gElBefore);
            layerEl.parentElement.insertBefore(layerEl, layerElBefore);
        }
    }
};

const moveLayerGroupUp = (gEl, e) => {
    if(gEl){
        stopDefault(e);
        var index = getGroupIndex(gEl);
        index = index > 0 ? index - 1 : 0;
        moveLayerGroupToIndex(gEl, index);
    }
};

const moveLayerGroupDown = (gEl, e) => {
    if(gEl){
        stopDefault(e);
        const maxGroupIndex = getMaxGroupIndex();
        var index = getGroupIndex(gEl);
        index = index < maxGroupIndex - 1 ? index + 1 : maxGroupIndex;
        moveLayerGroupToIndex(gEl, index);
    }
};

const handleSvgClick = (e) => {
    if(!closeColorSlider(e)){
        const svgEl = getSvgEl();
        const svgClickCoords = getClickLocationOnElement(e, svgEl);
        addPoint(svgClickCoords.x, svgClickCoords.y);
    }
};

const handleSvgRightClick = (e) => {
    if(!closeColorSlider(e)){
        deselectGroups(e);
    }
};

const moveCropHandle = (cropHandleEl, targetXY) => {
    const whichCrop = getWhichCropHandle(cropHandleEl);
    const box = getCropBoxEls();

    const moveBox = (xOrY, el, pos) => {
        if(!isNaN(targetXY[xOrY])){
            const bounds = box[el].getBoundingClientRect();
            if(targetXY[xOrY] != bounds[pos]){

                let diff = bounds[pos] - targetXY[xOrY];
                if(el === 'left' || el === 'top') {
                    diff = targetXY[xOrY] - bounds[pos];
                }

                const newSize = box[xOrY].int - Math.ceil(diff);

                setCropAxisValue(xOrY, newSize, box);
            }
        }
    };

    switch(whichCrop){
        case 'left': moveBox('x', 'left', 'right'); break;
        case 'top': moveBox('y', 'top', 'bottom'); break;
        case 'right': moveBox('x', 'right', 'left'); break;
        case 'bottom': moveBox('y', 'bottom', 'top'); break;
    }
};

const moveEachPolygonPoint = (gEl, moveXY) => {
    var newPointsStr = '';
    gEl.querySelectorAll('circle').forEach(circle => {
        var x = getPointX(circle) + moveXY.x;
        var y = getPointY(circle) + moveXY.y;

        circle.setAttributeNS(null, 'cx', x);
        circle.setAttributeNS(null, 'cy', y);

        newPointsStr+=`${x},${y} `;
    });
    gEl.querySelector('polygon').setAttributeNS(null, 'points', newPointsStr.trim());
};

const movePolygon = (gEl, moveXY) => {
    selectGroup(gEl);
    moveEachPolygonPoint(gEl, moveXY);
    updateForChanges();
};

const movePoint = (circleEl, moveXY) => {
    circleEl.classList.add('moving');
    const gEl = circleEl.parentElement;
    selectGroup(gEl);

    const polygon = gEl.querySelector('polygon');
    if(polygon) {
        var newPointsStr = '';
        gEl.querySelectorAll('circle').forEach(circle => {
            var x = getPointX(circle);
            var y = getPointY(circle);

            if(circle.classList.contains('moving')) {
                x += moveXY.x;
                y += moveXY.y;

                circle.setAttributeNS(null, 'cx', x);
                circle.setAttributeNS(null, 'cy', y);
            }

            newPointsStr+=`${x},${y} `;
        });
        polygon.setAttributeNS(null, 'points', newPointsStr.trim());
    }

    circleEl.classList.remove('moving');

    updateForChanges();
};

const startColorSliding = (e) => {
    if(e.target.classList.contains('handle')){
        stopDefault(e);
        e.target.classList.add('handle-down');
    }
};

const stopColorSliding = () => {
    const colorSliderHandle = getColorSliderHandleDownEl();
    if(colorSliderHandle) {
        colorSliderHandle.classList.remove('handle-down');
    } 
};

const setGroupHughByIndex = (index) => {
    if(index >= 0 && index < grayHues.length) {
        const colorSliderDiv = getOrCreateColorSliderEl();

        const colorHex = getColorSliderHexEl().innerHTML;

        const groupIndex = colorSliderDiv.attributes['data-group-index'].value;

        const gEl = getGroupElByIndex(groupIndex);
        gEl.querySelector('polygon').style.fill = colorHex;

        const layerEl = getLayerElByIndex(groupIndex);
        layerEl.querySelector('.hue').style.backgroundColor = colorHex;
    }
}; 

const setColorHexHughIndex = (index) => {
    if(index >= 0 && index < grayHues.length) {
        const hexEl = getColorSliderHexEl();
        hexEl.innerHTML = grayHues[index];
    }
};

const setColorSliderHughIndex = (index) => {
    if(index >= 0 && index < grayHues.length) {
        const handle = getColorSliderHandleEl();
        const rangeX = getColorSliderRange(handle);
        const absMax = Math.abs(rangeX.min) + Math.abs(rangeX.max);
        const percent = index / (grayHues.length - 1);
        const sum = percent * absMax;
        const newLeft = rangeX.min + sum;
        handle.style.left = newLeft + 'px';
    }
};

const setColorInputHughIndex = (index) => {
    if(index >= 0 && index < grayHues.length) {
        const input = getColorSliderInputEl();
        input.value = index;
    }
};

const getColorSliderRange = (colorSliderHandle, e) => {
    const sliderBar = colorSliderHandle.parentElement.parentElement;
    const sliderBarPos = sliderBar.getBoundingClientRect();
    const sliderWidth = sliderBar.clientWidth;

    const halfHandleWidth = colorSliderHandle.clientWidth / 2;

    const lowerLimitX = sliderBarPos.x - halfHandleWidth - sliderBarPos.x;
    const upperLimitX = (sliderBarPos.x + sliderWidth) - halfHandleWidth - sliderBarPos.x;

    var newLeft = e != undefined ? e.clientX - halfHandleWidth - sliderBarPos.x : -1;

    if(newLeft < lowerLimitX) { newLeft = lowerLimitX; }
    if(newLeft > upperLimitX) { newLeft = upperLimitX; }

    return {min: lowerLimitX, max: upperLimitX, leftAtCursor: newLeft};
};

const alignColorSliderWithMouse = (e, colorSliderHandle) => {
    const rangeX = getColorSliderRange(colorSliderHandle, e);

    colorSliderHandle.style.left = rangeX.leftAtCursor + 'px';

    const negativeOffset = rangeX.min < 0 ? Math.abs(rangeX.min) : 0;
    const percent = (rangeX.leftAtCursor + negativeOffset) / (rangeX.max + negativeOffset);
    const hueIndexPercent = (grayHues.length - 1) * percent;
    const hueIndex = Math.round(hueIndexPercent);

    setColorInputHughIndex(hueIndex);
    setColorHexHughIndex(hueIndex);
    setGroupHughByIndex(hueIndex);
};

const handleDocumentMousemove = (e) => {
    const colorSliderHandle = getColorSliderHandleDownEl();
    const mousemoveEl = getMousedownEl();
    const cropHandleEl = getCropHandleDownEl();
    if(colorSliderHandle) {
        alignColorSliderWithMouse(e, colorSliderHandle);
    }else if(mousemoveEl) {
        const svgEl = getSvgEl();
        const newMousedownCoords = getClickLocationOnElement(e, svgEl);
        const diffCoords = getCoordsDifference(newMousedownCoords, prevMousedownCoords);
        switch(mousemoveEl.nodeName.toLowerCase()) {
            case 'polygon': movePolygon(mousemoveEl.parentElement, diffCoords); break;
            case 'circle': movePoint(mousemoveEl, diffCoords); break;
        };
        prevMousedownCoords = newMousedownCoords;
    }else if(cropHandleEl) {
        moveCropHandle(cropHandleEl, {x: e.clientX, y: e.clientY});
    }
};

const handleDocumentMouseup = (e) => {
    stopColorSliding();
    stopDragging();
};

const handleMirrorXBtnClick = (e) => {
    stopDefault(e);
    if(hasEnableMirrorX()) {
        document.body.classList.remove('mirror-x');
    }else{
        document.body.classList.add('mirror-x');
    }
    updateForChanges();
}; 

const handleSquareCropBtnClick = (e) => {
    stopDefault(e);
    if(hasEnableSquareCrop()) {
        document.body.classList.remove('square-crop');
    }else{
        document.body.classList.add('square-crop');
    }
    updateCropBox();
}

const updateForChanges = () => {
    updateMirrorX();
    exportAsImage();
};

const updateMirrorX = () => {
    if(hasEnableMirrorX()){
        const mirrorDiv = getMirrorXAreaEl();
        const svgEl = getSvgEl();
        mirrorDiv.innerHTML = `<svg id="mirror_x_svg" style="transform: scaleX(-1);" xmlns="${svgNs}">${svgEl.innerHTML}</svg>`;
    }else{
        getMirrorXAreaEl().innerHTML = '';
    }
};

const resetCropValues = () => {
    const box = getCropBoxEls();

    box.top.style.height = '';
    box.bottom.style.height = '';
    box.left.style.width = '';
    box.right.style.width = '';
};

const clampCropSize = (axis, size) => {
    size = isNaN(size) ? parseInt(size) : size;
    if(size <= 0) { return 0; }
    
    switch(axis){
        case 'x': fullSize = getSvgAreaEl().clientWidth; break;
        case 'y': fullSize = getSvgAreaEl().clientHeight; break;
    }

    const maxSize = fullSize - (getCropHandleEl('left').clientWidth * 2);
    if(size > maxSize) { 
        size = maxSize; 
    }

    return size;
};

const setCropAxisValue = (axis, size, box) => {
    const rightOrTop = axis === 'x' ? 'right' : 'top';
    const leftOrBottom = axis === 'x' ? 'left' : 'bottom';
    const widthOrHeight = axis === 'x' ? 'width' : 'height';
    const inputXOrY = axis === 'x' ? ['inputX', 'inputY'] : ['inputY', 'inputX'];
    const otherAxis = axis === 'x' ? 'y' : 'x';

    size = clampCropSize(axis, size);

    const boxData = box ? box : getCropBoxEls();

    const clientSize = 'client' + widthOrHeight[0].toUpperCase() + widthOrHeight.slice(1);
    const fullSize = getSvgAreaEl()[clientSize];
    const cropSum = fullSize - size;
    const halfCropSum = cropSum / 2;

    boxData[leftOrBottom].style[widthOrHeight] = halfCropSum + 'px';
    boxData[rightOrTop].style[widthOrHeight] = halfCropSum + 'px';

    boxData[inputXOrY[0]].value = size;
    
    if(hasEnableSquareCrop() && boxData[otherAxis].int !== size){
        setCropAxisValue(otherAxis, size);
    }else{
        boxData[inputXOrY[1]].value = boxData[otherAxis].int;
    }

    exportAsImage();
};

const setCropXValue = (size, box) => { setCropAxisValue('x', size, box); };

const setCropYValue = (size, box) => { setCropAxisValue('y', size, box); };

const keepCropSquare = () => {
    resetCropValues();
    const box = getCropBoxEls();

    if(box.x.int > box.y.int) {
        setCropXValue(box.y.int);
    }else if(box.x.int < box.y.int) {
        setCropYValue(box.x.int);
    }
}

const updateCropInputsFromResizedBox = () => {
    const box = getCropBoxEls();
    box.inputX.value = box.x.int;
    box.inputY.value = box.y.int;
    exportAsImage();
};

const updateCropBox = () => {
    if(hasEnableSquareCrop()) {
        keepCropSquare();
    }else{
        updateCropInputsFromResizedBox();
    }
};

const handleKeydown = (e) => {
    switch(e.keyCode) {
        case 13: // enter
            if(!closeColorSlider(e)) {
                const focusEl = getFocusEl();
                if(focusEl) {
                    stopDefault(e);
                    openColorSliderAtCursor(focusEl.parentElement);
                }
            }
            break;
        case 32: // spacebar
            if(!closeColorSlider(e)) {
                const focusEl = getFocusEl();
                if(focusEl) {
                    stopDefault(e);
                    openColorSliderAtCursor(focusEl.parentElement);
                }
            }
            break;
        case 27: // escape
            if(!closeColorSlider(e)) {
                if(!deleteFocused(e)) {
                    deselectGroups(e);
                } 
            }
            break;
        case 8: // backspace
            if(!closeColorSlider(e)) {
                deleteFocused(e); break;
            }
        case 46: // delete
            deleteFocused(e); break;
        case 68: // D
            duplicatePolyAroundSelected(); break;
        case 67: // C
            createCloneOfGroup(); break;
        case 88: // X
            deleteFocused(e); break;
        case 38: // up arrow
            if(!colorSliderIsOpen()) {
                moveLayerGroupUp(getSelectedGroupEl(), e); 
            }
            break;
        case 40: // down arrow
            if(!colorSliderIsOpen()) {
                moveLayerGroupDown(getSelectedGroupEl(), e); 
            }
            break;
        case 39: // right arrow
            if(!colorSliderIsOpen()) {
                moveLayerGroupDown(getSelectedGroupEl(), e); 
            }
            break;
        case 37: // left arrow 
            if(!colorSliderIsOpen()) {
                moveLayerGroupUp(getSelectedGroupEl(), e); 
            }
            break;
    }
};

const stopDragging = () => {
    const mousedownEl = getMousedownEl();
    const cropHandleEl = getCropHandleDownEl();
    if(mousedownEl) {
        mousedownEl.classList.remove('mousedown');
        prevMousedownCoords = false;
    }else if(cropHandleEl){
        cropHandleEl.classList.remove('crop-handle-down');
        prevMousedownCoords = false;
    }
};

const handleDocumentMouseLeave = (e) => {
    stopDragging();
};

const handleWindowResize = (e) => {
    updateCropBox();
};

const handleCropHandleMouseDown = (e) => {
    e.target.classList.add('crop-handle-down');
};

const handleCropXInputChange = (e) => {
    setCropXValue(parseInt(e.target.value));
};

const handleCropYInputChange = (e) => {
    setCropYValue(parseInt(e.target.value));
};

const attachCropHandleEvents = () => {
    const cropHandle = getCropHandleEls();
    cropHandle.left.addEventListener('mousedown', handleCropHandleMouseDown);
    cropHandle.top.addEventListener('mousedown', handleCropHandleMouseDown);
    cropHandle.right.addEventListener('mousedown', handleCropHandleMouseDown);
    cropHandle.bottom.addEventListener('mousedown', handleCropHandleMouseDown);
};

const attachEvents = () => {
    const svgEl = getSvgEl();
    svgEl.addEventListener('click', handleSvgClick);
    svgEl.addEventListener('contextmenu', handleSvgRightClick);

    const mirrorXDiv = getMirrorXAreaEl();
    mirrorXDiv.addEventListener('click', stopDefault);
    mirrorXDiv.addEventListener('contextmenu', stopDefault);

    getSquareCropBtnEl().addEventListener('click', handleSquareCropBtnClick);

    getMirrorXBtnEl().addEventListener('click', handleMirrorXBtnClick);

    document.addEventListener('keydown', handleKeydown);
    document.addEventListener('mousemove', handleDocumentMousemove);
    document.addEventListener('mouseup', handleDocumentMouseup);
    document.addEventListener('mouseleave', handleDocumentMouseLeave);

    getCropXInputEl().addEventListener('change', handleCropXInputChange);
    getCropYInputEl().addEventListener('change', handleCropYInputChange);

    attachCropHandleEvents();

    window.addEventListener('resize', handleWindowResize);
};

document.addEventListener('DOMContentLoaded', (e) => {

    initOnce();
    attachEvents();

});