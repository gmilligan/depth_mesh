module.exports = (app, rootPath, allowedPaths) => {

    const fs = require('fs');
    const pathUtil = require('path');

    var watching = {}; // <--- all the files being watched

    const getChanged = () => {
        var changes = [];
        Object.keys(watching).forEach((path)=>{

            const stats = fs.statSync(rootPath + path);
            watching[path]['newMod']=stats.mtime + '';

            if(watching[path]['lastMod']!=watching[path]['newMod']){
                watching[path]['lastMod']=watching[path]['newMod'];
                changes.push(watching[path]);
            }
        });

        return changes;
    };

    const hasPath = (path) => path && path.length > 0;
    const isNewPath = (path) => !watching.hasOwnProperty(path);
    const pathExists = (path) => fs.existsSync(rootPath + path);
    const isFile = (path) => fs.lstatSync(rootPath + path).isFile();
    const validFileType = (path) => allowedPaths.includes(pathUtil.extname(path).toLowerCase());

    const newPathStatus = (path) => {
        if(!hasPath(path)){ return 'error - no path'; }
        if(!isNewPath(path)) { return 'error - already watching'; }
        if(!pathExists(path)){ return 'error - file does not exist'; }
        if(!isFile(path)){ return 'error - not a file'; }
        if(!validFileType(path)){ return 'error - file type not allowed'; }
        return 'ok';
    }

    const unwatchAll = () => {
        watching = {};
    };

    const newwatch = (path) => {
        watching[path] = {path: path, lastMod: ''};
    };

    app.post('/newwatch', (req,res) => { 
        const path = req.body.path;
        if(!req.body.isWatching){
            unwatchAll(); // <--- reset (no watching) just like current frontend state
        }
        const status = newPathStatus(path);
        const ret = {
            status: status,
            path: path
        }
        if(status=='ok'){
            newwatch(path);
        }
        res.send(JSON.stringify(ret));
    });

    app.get('/pingwatch', (req, res) => {
        const changes = getChanged();
        const ret = {
            status: 'ok',
            changes: changes
        }
        res.send(JSON.stringify(ret));
    });

};
