module.exports = (app, rootPath, allowedPaths) => {

    const fs = require('fs');
    const pathUtil = require('path');

    const getParentDir = (path) => path.indexOf('/') != -1 ? path.substring(0, path.lastIndexOf('/') + 1) : path;

    const pathExists = (path) => fs.existsSync(rootPath + path);
    const isDirectory = (path) => fs.lstatSync(rootPath + path).isDirectory();
    const validFileType = (path) => allowedPaths.includes(pathUtil.extname(path).toLowerCase());
    
    const getSaveStatus = (data) => {
        if (!data.hasOwnProperty('img')) { return 'error - missing "img" property'; }
        if (!data.hasOwnProperty('path')) { return 'error - missing "path" property'; }
        if (data.img.length < 1) { return 'error - "img" property is empty'; }
        if (data.path.length < 1) { return 'error - "path" property is empty'; }

        const parentDir = getParentDir(data.path);

        if(!pathExists(parentDir)) { return 'error - parent directory does not exist'; }
        if (!isDirectory(parentDir)) { return 'error - not a directory'; }
        if(!validFileType(data.path)){ return 'error - save-image type not allowed'; }

        return 'ok';
    };

    app.post('/saveimage', (req,res) => { 
        const status = getSaveStatus(req.body);

        let base64img = req.body.img;
        const path = req.body.path;
        const parentDir = getParentDir(path);

        let ret = { 
            status: status,
            parentDir: parentDir,
            path: path,
            rootPath: rootPath, 
            err: null
        };
        
        if(status === 'ok') {
            base64img = base64img.replace(/^data:image\/png;base64,/, '');
            fs.writeFile(rootPath + path, base64img, 'base64', (err) => {
                ret['err'] = err;
                res.send(JSON.stringify(ret));
            });
        }
    });
};